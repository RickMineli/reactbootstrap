const urlApi = 'https://g7kv8s1hr4.execute-api.us-east-1.amazonaws.com/pro'

var axios = require('axios')

const getAll = axios.get(urlApi + '/clientes')
  .then(function (response) {
    return response.data
  })
  .catch(function (error) {
    console.log(error)
  })

  const save = (cliente = {}) => {
    if (!cliente.id) return axios.post(urlApi + '/clientes', cliente)
    if (cliente.id) return axios.put(urlApi + '/clientes', cliente)
  }
  
  const get = (id) => {
    return axios.get(urlApi + '/clientes/' + id)
  }
  
  const remove = (id) => {
    return axios.delete(urlApi + '/clientes', { data: { id } })
  }
  
  export { getAll, save, get, remove }
  