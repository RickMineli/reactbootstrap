const urlApi = 'https://g7kv8s1hr4.execute-api.us-east-1.amazonaws.com/pro'

var axios = require('axios')

const getAll = axios.get(urlApi + '/produtos')
  .then(function (response) {
    return response.data
  })
  .catch(function (error) {
    console.log(error)
  })

  const save = (produto = {}) => {
    if (!produto.id) return axios.post(urlApi + '/produtos', produto)
    if (produto.id) return axios.put(urlApi + '/produtos', produto)
  }
  
  const get = (id) => {
    return axios.get(urlApi + '/produtos/' + id)
  }
  
  const remove = (id) => {
    return axios.delete(urlApi + '/produtos', { data: { id } })
  }
  
  export { getAll, save, get, remove }
  
