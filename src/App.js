import React, { Component } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import Pedidos from './pages/Pedidos'
import Produtos from './pages/Produtos'
import Clientes from './pages/Clientes'
import Header from './components/Header'

import 'bootstrap/dist/css/bootstrap.css'
import CadastroProdutos from './pages/CadastroProdutos';
import CadastroClientes from './pages/CadastroClientes';

class App extends Component {
  render () {
    return (
      <BrowserRouter>
        <div>
          <Header />
          <Switch>
            <Route path='/' exact component={Pedidos} />
            <Route path='/produtos' exact component={Produtos} />
            <Route path='/produtos/cadastro' exact component={CadastroProdutos} />
            <Route path='/clientes' exact component={Clientes} />
            <Route path='/clientes/cadastro' exact component={CadastroClientes} />
          </Switch>
        </div>
      </BrowserRouter>
    )
  }
}

export default App
