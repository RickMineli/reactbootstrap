import React, { Component } from 'react'

import { Link } from 'react-router-dom'

export default class Header extends Component {
  render () {
    return (
      <div>
        <Link to='/'>Pedidos</Link>
        <Link to='/clientes'>Clientes</Link>
        <Link to='/produtos'>Produtos</Link>
      </div>
    )
  }
}
