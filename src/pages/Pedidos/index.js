import React, { Component } from 'react'
import { Table } from 'react-bootstrap'


export default class Pedidos extends Component {
  state = {
    loading: true,
    pedidos: []
  }

  async componentWillMount(){
    this.setState({
      loading: false
    })
    console.log(this.state.pedidos)
  }

  render () {
    return (
      <div>
        { this.state.loading && (
          <p>Está carregando os pedidos...</p>
        )}
        
        { !this.state.loading && (
          <div>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nome</th>
                  <th>Descrição</th>
                  <th>Preço</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                { this.state.pedidos.map((pedido) => (
                  <tr key={pedido.id}>
                    <td>{pedido.id}</td>
                    <td>{pedido.nome}</td>
                    <td>{pedido.descricao}</td>
                    <td>{pedido.preco}</td>
                    <td>{pedido.status}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
        )}
      </div>
    )
  }
}
