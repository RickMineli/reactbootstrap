import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import {Row, Col, Form, Button } from 'react-bootstrap'

import * as serviceCliente from '../../services/serviceCliente'
export default class CadastroCliente extends Component {
        state = {
          loading: false,
          nome: '',
          endereco: '',
          email: '',
          fone: ''
        }
      
        async componentWillMount() {
            try {
              if(this.props.match.params.id) {
                  const { data } = await serviceCliente.get(this.props.match.params.id)
                  console.log(data)
                  this.setState({
                      loading: false,
                      ...data,
                  })
              }
            } catch (error) {
                alert('não recuperou')
                console.log(error)
            }
        }
      
        updateField = (event) => {
          this.setState({
              [event.target.id] : event.target.value
          })
        }
      
        save = async () => {
            try {
              if(!this.state.nome) return alert('Digite um nome!')
              if(!this.state.email) return alert('Digite um email!')
      
              this.setState({
                  loading: true
              })
      
              await serviceCliente.save(this.state)
              
              this.setState({
                  loading: false,
                  redirect: true
              })    
            } catch (error) {
                this.setState({
                    loading: false
                })
                alert('deu ruim')
                console.log(error)
            }
            
        }
      
        render () {
          return (
            <div>
      
              { this.state.redirect && <Redirect to='/clientes' />}
      
              { this.state.loading && (
                <p>Carregando clientes...</p>
              )}
              
              { !this.state.loading && (
                <div>
                  <div className='toolbar'>
                  <Row>
                    <Col>
                      <Form.Control type='text' placeholder='Digite o nome' id='nome' onChange={this.updateField} value={this.state.nome} />   
                    </Col>
                  </Row>
      
                  <Row>
                    <Col>
                      <Form.Control type='email' placeholder='Digite o email' id='email' onChange={this.updateField} value={this.state.email} />   
                    </Col>
                  </Row>
      
                  <Row>
                    <Col>
                      <Form.Control type='number' placeholder='Digite o fone' id='fone' onChange={this.updateField} value={this.state.fone} />   
                    </Col>
                  </Row>
      
                  <Row>
                    <Col>
                      <Form.Control type='text' placeholder='Digite o endereco' id='endereco' onChange={this.updateField} value={this.state.endereco} />   
                    </Col>
                  </Row>
      
                  <Row>
                    <Col>
                      <Button onClick={this.save}>Salvar</Button>
                    </Col>
                  </Row>
                  
                  </div>
                  
                </div>
              )}
            </div>
          )
        }
      }
      