import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import {Row, Col, Form, Button } from 'react-bootstrap'

import * as serviceProdutos from '../../services/serviceProdutos'
export default class CadastroProdutos extends Component {

  state = {
    loading: false,
    nome: '',
    descricao: '',
    preco: '',
    estoque: '',
    status: 'ATIVO',
  }

  async componentWillMount(){
    try {
      if(this.props.match.params.id){
        const { data } = await serviceProdutos.get(this.props.match.params.id)
        console.log(data)
        this.setState({
          loading: false,
          ...data,
        })
      }
    } catch (error) {
      alert('Não foi possivel recuperou os dados do Produto')
      console.log(error)
    }
  }

  updateFields = (event) => {
    this.setState({
      [event.target.id] : event.target.value
    })
  }

  save = async () => {
    try {
      if(!this.state.nome) return alert('Informe um nome para o Produto!')
      if(!this.state.descricao) return alert('Informe uma descrição para o Produto!')
      if(!this.state.preco) return alert('Informe um preço para o Produto')
      if(!this.state.estoque){ this.state.estoque = 0.0 }
      if(!this.state.status) return alert('Informe um status para o produto')
      this.setState({
        loading: true
      })
      await serviceProdutos.save(this.state)
      this.setState({
        loading: false,
        redirect: true
      })

    } catch (error) {
      this.setState({
        loading:false
      })
      alert('Não foi possivel salvar o cadastro do produto')
      console.log(error)
    }
  }
  render() {
    return (
      <div>
        { this.state.redirect && <Redirect to='/produtos' /> }
        { this.state.loading && (
          <p>Carregando clientes...</p>
        )}
        { !this.state.loading && (
          <div>
            <div className='toolbar'>
            <Row>
              <Col>
                <Form.Control type='text' placeholder='Nome do Produto' id='nome' onChange={this.updateFields} value={this.state.nome} />
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Control type='text' placeholder='Descição do Produto' id='descricao' onChange={this.updateFields} value={this.state.descricao} />
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Control type= 'number' placeholder='Preço do Produto' id='preco' onChange={this.updateFields} value={this.state.preco} />
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Control type='number' placeholder='Estoque atual do Produto' id='estoque' onChange={this.updateFields} value={this.state.estoque} />
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Group controlId="status">
                  <Form.Label>Status atual do Produto</Form.Label>
                  <Form.Control value={this.state.status} onChange={this.updateFields} placeholder='Status atual do Produto' as="select">
                    <option>ATIVO</option>
                    <option>INATIVO</option>
                    <option>BLOQUEADO</option>
                  </Form.Control>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col>
                <Button onClick={this.save}>Salvar Produto</Button>
              </Col>
            </Row>

            </div>
          </div>
        )}
          
          {}
      </div>
    )
  }
}