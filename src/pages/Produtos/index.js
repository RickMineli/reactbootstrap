import React, { Component } from 'react'
import { Row, Form , Button, Table, Col } from 'react-bootstrap';
import { FaEdit, FaTrash } from 'react-icons/fa';
import { Link } from 'react-router-dom'

import * as serviceProdutos from '../../services/serviceProdutos'

export default class Produtos extends Component {
  state = {
    loading: true,
    produtos: [],
    search:''
  }

  async componentWillMount(){
    this.setState({
      loading: false,
      produtos: await serviceProdutos.getAll
    })
  }

  updateSearch = (event) => {
    this.setState({
      search: event.target.value
    })
  };

  remove = async (id) => {
    try {
      await serviceProdutos.remove(id)
      this.componentWillMount()
    } catch (error) {
      alert('Não foi possível exlcuir o produto!')
    }
  }
  

  render () {
    var filteredProdutos = this.state.produtos.filter((produto) => {
      return produto.nome.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1;
    });

    return (
      <div>
        { this.state.loading && (
          <p>Carregando produtos...</p>
        )}
        
        { !this.state.loading && (
          <div>
            <div className= 'toolbar'>
            <Row>
              <Col>
                <Form.Control type='search' onChange={this.updateSearch} placeholder='Procurar pelo nome...' /> 
              </Col>
              <Col>
                <Link className='btn btn-primary float-right' to='/produtos/cadastro'>Novo Produto</Link>
              </Col>
            </Row>
          </div>

   
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nome</th>
                  <th>Descrição</th>
                  <th>Preço</th>
                  <th>Estoque</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                { filteredProdutos.map((produto) => (
                  <tr key={produto.id}>
                    <td>{produto.id}</td>
                    <td>{produto.nome}</td>
                    <td>{produto.descricao}</td>
                    <td>{produto.preco}</td>
                    <td>{produto.estoque}</td>
                    <td>{produto.status}</td>
                    <td>
                      <Link to={`/produtos/cadastro/${produto.id}`} >
                        <Button className='float-right'><FaEdit/></Button>
                      </Link>               
                        <Button className='float-right' onClick={() => this.remove(produto.id)}><FaTrash/>
                      </Button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
        )}
      </div>
    )
  }
}
