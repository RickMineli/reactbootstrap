import React, { Component } from 'react'
import { Row, Form , Button, Table, Col } from 'react-bootstrap';
import { FaEdit, FaTrash } from 'react-icons/fa';
import { Link } from 'react-router-dom'

import * as serviceClientes from '../../services/serviceCliente'

export default class Clientes extends Component {
  state = {
    loading: true,
    clientes: [],
    search:''
  }

  async componentWillMount(){
    this.setState({
      loading: false,
      clientes: await serviceClientes.getAll
    })
  }

  updateSearch = (event) => {
    this.setState({
      search: event.target.value
    })
  };

  remove = async (id) => {
    try {
      await serviceClientes.remove(id)
      this.componentWillMount()
    } catch (error) {
      alert('Não foi possível exlcuir o cliente!')
    }
  }
  

  render () {
    var filteredClientes = this.state.clientes.filter((cliente) => {
      return cliente.nome.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1;
    });

    return (
      <div>
        { this.state.loading && (
          <p>Carregando Clientes...</p>
        )}
        
        { !this.state.loading && (
          <div>
            <div className= 'toolbar'>
            <Row>
              <Col>
                <Form.Control type='search' onChange={this.updateSearch} placeholder='Procurar pelo nome...' /> 
              </Col>
              <Col>
                <Link className='btn btn-primary float-right' to='/Clientes/cadastro'>Novo cliente</Link>
              </Col>
            </Row>
          </div>
          <Table striped bordered hover>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nome</th>
                  <th>CPF</th>
                  <th>E-mail</th>
                  <th>Telefone</th>
                  <th>Endereço</th>
                  <th>CEP</th>
                  <th>Cidade</th>
                  <th>UF</th>
                  <th>Observação</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                { filteredClientes.map((cliente) => (
                  <tr key={cliente.id}>
                    <td>{cliente.id}</td>
                    <td>{cliente.nome}</td>
                    <td>{cliente.cpf}</td>
                    <td>{cliente.email}</td>
                    <td>{cliente.fone}</td>
                    <td>{cliente.endereco}</td>
                    <td>{cliente.cep}</td>
                    <td>{cliente.cidade}</td>
                    <td>{cliente.uf}</td>
                    <td>{cliente.observacao}</td>
                    <td>{cliente.status}</td>
                    <td>
                      <Link to={`/Clientes/cadastro/${cliente.id}`} >
                        <Button className='float-right'><FaEdit/></Button>
                      </Link>               
                        <Button className='float-right' onClick={() => this.remove(cliente.id)}><FaTrash/>
                      </Button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
        )}
      </div>
    )
  }
}
